variable "region" {
  type        = string
  description = "AWS Region of deployment"
}

variable "subnet_id" {
  type        = string
  description = "AWS Subnet ID for the deployment"
}

variable "user_data" {
  type        = string
  description = "coreos config ignition file. passed in as userdata"
}

variable "app_port" {
  type        = string
  description = "port to access the app on. unencrypted"
}

variable "root_domain" {
  type        = string
  description = "Your root domain. eg: thisisanexample.com"
}

variable "subdomain" {
  type        = string
  description = "Subdomain through which you will be able to access the code server"
}

variable "instance_size" {
  type        = string
  description = "instance size for the code server deployment"
}

variable "ami" {
  type        = string
  description = "Image for the instance deployment"
}

variable "app_name" {
  type        = string
  description = "Name for the deployment"
}

variable "cloudflare_api_token" {
  type        = string
  description = "Cloudflare API token"
}
